define([
  'jquery',
  'underscore',
  'backbone',

  'maincontainer',
  'login'],
  function($, _, Backbone, Main, Login){ 
    'use strict';
    
    
    var App = function(){};
    
    _.extend(App.prototype, {
      initialize : function(){        
        var main = new Main();
        var login = new Login();
        main.$el.append(login.render().el);       
        
        this.icheck();        
      },
      
      icheck: function(){
        if($(".icheck-me").length > 0){
          $(".icheck-me").each(function(){
            var $el = $(this);
            var skin = ($el.attr('data-skin') !== undefined) ? "_"+$el.attr('data-skin') : "",
            color = ($el.attr('data-color') !== undefined) ? "-"+$el.attr('data-color') : "";

            var opt = {
              checkboxClass: 'icheckbox' + skin + color,
              radioClass: 'iradio' + skin + color,
              increaseArea: "10%"
            }

            $el.iCheck(opt);
          });
        }
      }
    });
    
    return App;
  });