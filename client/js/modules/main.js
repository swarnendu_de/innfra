define([
  'jquery',
  'underscore',
  'backbone'],
  function($, _, Backbone){ 
    'use strict';
  
    var Main = Backbone.View.extend({
      el : '#main_container'
    });
    
    return Main;
  });