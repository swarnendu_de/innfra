define([
  'jquery',
  'underscore',
  'backbone'],
  function($, _, Backbone){ 
    'use strict';
    
    var LoginRouter = Backbone.Router.extend({
      routes: {
        '' : 'init',
        'login' : 'showLogin',
        'signup' : 'showSignUp',
        'forgotpass' : 'showForgotPass',
        
        '*actions': 'defaultRoute'      
      },
      
      init : function(){
        // If already logged in, let user in. Else to login page.
        this.navigate('login', {
          trigger: true
        });
      },
      
      showLogin : function(){
        // Check user already loggedin or not          
        console.log('login');
      },
      
      showSignUp: function(){
        
      },
      
      showForgotPass : function(){
        
      },
      
      defaultRoute : function(){        
        this.navigate('', {
          trigger: true
        });
      }
    });
    
    return LoginRouter;
  });