//Filename: boilerplate.js 
define([
  'jquery',
  'underscore',
  'backbone', 
  'handlebar',
  'text!../tpl/user/login.html', 'icheck'],
  function($, _, Backbone, HandleBar, loginTpl, icheck){ 
    'use strict';
  
    var Login = Backbone.View.extend({
      tagName : 'div',
      className : 'login',
    
      render : function(){
        var tpl = HandleBar.compile(loginTpl);
        this.$el.html(tpl());
        return this;
      }
    });
    
    return Login;
  });