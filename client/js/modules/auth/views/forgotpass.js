//Filename: boilerplate.js 
define([
  'jquery',
  'underscore',
  'backbone', 
  'handlebar',
  'text!../tpl/user/forgotpass.html'],
  function($, _, Backbone, HandleBar, forgotPassTpl){ 
    'use strict';
  
    var ForgotPass = Backbone.View.extend({
      tagName : 'div',
      className : 'login',
    
      render : function(){
        var tpl = HandleBar.compile(forgotPassTpl);
        this.$el.html(tpl());
        return this;
      }
    });
    
    return ForgotPass;
  });