//Filename: boilerplate.js 
define([
  'jquery',
  'underscore',
  'backbone', 
  'handlebar',
  'text!../tpl/user/signup.html'],
  function($, _, Backbone, HandleBar, signupTpl){ 
    'use strict';
  
    var SignUp = Backbone.View.extend({
      tagName : 'div',
      className : 'login',
    
      render : function(){
        var tpl = HandleBar.compile(signupTpl);
        this.$el.html(tpl());
        return this;
      }
    });
    
    return SignUp;
  });