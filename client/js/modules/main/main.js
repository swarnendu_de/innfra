define([
  'jquery',
  'underscore',
  'backbone',
  'maincontainer'],

  function($, _, Backbone, MainContainer){ 
    'use strict';
    
    return {
      init : function(){
        new MainContainer().render();
        
      }
    };
  });