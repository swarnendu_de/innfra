// Filename: main.js 
// Require.js allows us to configure shortcut alias 
// There usage will become more apparent further along in the tutorial. 
require.config({
  //By default load any module IDs from js/lib
  baseUrl: 'js',
  
  //except, if the module ID starts with "app",
  //load it from the js/app directory. paths
  //config is relative to the baseUrl, and
  //never includes a ".js" extension since
  //the paths config could be for a directory.
  paths: {
    app : 'app',
    underscore: 'lib/underscore/underscore', 
    backbone: 'lib/backbone/backbone',
    jquery : 'lib/jquery-2.0.1',
    text : 'lib/text/text',
    bootstrap : 'lib/bootstrap/js/bootstrap',
    handlebar : 'lib/handlebars.js/dist/handlebars',
    icheck : 'lib/flatui/plugins/icheck/jquery.icheck.min',
    eakroko : '',
    
    // Publisher 
    publisher : 'publisher',
    
    // Routers
    loginRouter : 'router/login',
    
    // Views
    maincontainer : 'views/main',
    login : 'views/user/login',
    forgotPass : 'views/user/forgotpass',
    signup : 'views/user/signup'
  },
  
  shim : {
    jquery : 'jquery',
    bootstrap : 'bootstrap',
    icheck : {
      deps: ['jquery'],
      exports: 'icheck'
    }
  }
}); 


require(['jquery', 'app', 'loginRouter'], function($, App, LoginRouter){    
  $(document).ready(function(){
    // Initialize the login router
    new LoginRouter();
    Backbone.history.start();
    
    // Initiate the app
    new App().initialize();
  });
});
