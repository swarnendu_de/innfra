var UserModel = require("../models/user").User;

// Get all the users
exports.findAll = function( request, response ) {
  return UserModel.find( function( err, users ) {
    if( !err ) {
      return response.send( users );
    } else {
      return console.log( err );
    }
  });
}

// Add an user
exports.addUser = function( request, response ) {
  var userData = request.body;
  
  var user = new UserModel({
    firstName: userData.firstName,
    lastName: userData.lastName,
    password: userData.password,
    email : userData.email
  });
    
  user.save( function( err ) {
    if( !err ) {
      return console.log( 'New user added.' );
    } else {
      return console.log( err );
    }
  });
    
  return response.send( user );
}

