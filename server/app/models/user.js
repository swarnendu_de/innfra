
/*!
 * Module dependencies
 */

var mongoose = require('mongoose'),
Schema = mongoose.Schema;

/**
 * User schema
 */

var User = new Schema({
  firstName: {
    type: String, 
    'default': ''
  },
  lastName: {
    type: String, 
    'default': ''
  },
  email: {
    type: String, 
    'default': ''
  },
  password: {
    type: String, 
    'default': ''
  },
  phone: {
    type: Number, 
    'default': ''
  },
  permanentAddress: {
    type: String, 
    'default': ''
  },
  presentAddress: {
    type: String, 
    'default': ''
  },
  role: {
    type: Number, 
    'default': ''
  }
});

/**
  * Add your
  * - pre-save hooks
  * - validations
  * - virtuals
  */

/**
 * Methods
 */

User.methods = {

  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} plainText
   * @return {Boolean}
   * @api public
   */

  authenticate: function (plainText) {
    return plainText === this.password;
//    return this.encryptPassword(plainText) === this.hashed_password;
  },

  /**
   * Make salt
   *
   * @return {String}
   * @api public
   */

  makeSalt: function () {
    return Math.round((new Date().valueOf() * Math.random())) + '';
  },

  /**
   * Encrypt password
   *
   * @param {String} password
   * @return {String}
   * @api public
   */

  encryptPassword: function (password) {
    if (!password){
      return ''
    }
    var encrypred;
    
    try {
      encrypred = crypto.createHmac('sha1', this.salt).update(password).digest('hex');
      return encrypred;
    } catch (err) {
      return '';
    }
  }
}

/**
* Statics
*/

User.static({

  });

/**
* Register
*/

module.exports = {
  User: mongoose.model('User', User)
};