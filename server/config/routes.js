
/*!
 * Module dependencies.
 */

var path = require('path');
var rootPath = path.resolve(__dirname + '../');

var base = require('../app/controllers/base'),
user = require('../app/controllers/user');

console.log('miao!')

/**
 * Expose
 */

module.exports = function (app, passport) {    
  /******************* Users **********************/ 
  
  // Get all users
  app.get('/api/users', user.findAll);
  
  // Add an user
  app.post('/api/users/add', user.addUser);
  
  
  // Handle all default routes
  app.get('/api/*', base.handleError);

}