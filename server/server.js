// Module dependencies.
var application_root = __dirname,
express = require( 'express' ), //Web framework
path = require( 'path' ), //Utilities for dealing with file paths
mongoose = require( 'mongoose' ), //MongoDB integration
routes = require('./config/routes'),
env = process.env.NODE_ENV || 'development',//user = require('./app/controllers/user'),
app = express();

// Configure server
app.configure( function() {
  //parses request body and populates request.body
  app.use( express.bodyParser() );

  //checks request.body for HTTP method overrides
  app.use( express.methodOverride() );

  //perform route lookup based on url and HTTP method
  app.use( app.router );
    
  //Where to serve static content
  app.use( express.static( path.join( application_root, 'public') ) );

  //Show all errors in development
  app.use( express.errorHandler({
    dumpExceptions: true, 
    showStack: true
  }));
});

// Enable CORS
app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

//Connect to database
mongoose.connect( 'mongodb://localhost/innofied_intranet' );

// Bootstrap routes
require('./config/routes')(app);

//Start server
var port = 3000;
app.listen( port, function() {
  console.log( 'Express server listening on port %d in %s mode', port, app.settings.env );
});

// Expose app
module.exports = app;